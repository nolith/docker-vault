#!/bin/sh
set -e

if [ "$1" = 'server' ]; then
  vault server -config=/config.hcl &
  sleep 1
  vault status || vault unseal $VAULT_KEY

  if [[ -d /init-db-data/ ]]; then
    for f in /init-db-data/*.sh
    do
      echo "****** Running $f ******"
      . $f
      echo "****** Done ******"
    done
  fi

  wait
fi

exec "$@"
