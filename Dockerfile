FROM progrium/busybox
MAINTAINER Alessio Caiazza <alessio@chorally.com>
ENV REFRESHED_AT 2016-02-22
ENV VAULT_VERSION 0.5.0
ENV CONSUL_CLI_VERSION 0.2.0

COPY content /

ADD https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip /tmp/vault.zip
ADD https://github.com/CiscoCloud/consul-cli/releases/download/v${CONSUL_CLI_VERSION}/consul-cli_${CONSUL_CLI_VERSION}_linux_amd64.tar.gz /tmp/consul-cli.tar.gz

RUN cd /bin && unzip /tmp/vault.zip && rm /tmp/vault.zip &&\
  gunzip -c /tmp/consul-cli.tar.gz | tar x -C /tmp/ &&\
  mv /tmp/consul-cli_${CONSUL_CLI_VERSION}_linux_amd64/consul-cli /bin/consul-cli &&\
  rm -rf /tmp/consul-cli_${CONSUL_CLI_VERSION}_linux_amd64/ &&\
  chmod +x /docker-entrypoint.sh /bin/vault /bin/consul-cli


VOLUME /data
EXPOSE 8200
ENV VAULT_ADDR "http://127.0.0.1:8200"
ENV VAULT_KEY 37d0fbd6c7699d22aedd96d280fa8b9d48bc0b7ed4ec11d3e1db547a933870cf
ENV VAULT_TOKEN 89ef8bdb-ce05-b7fd-bbde-f4e2ffbaadc6

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["server"]
